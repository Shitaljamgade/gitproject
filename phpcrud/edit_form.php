<?php

  require("dbconn.php");
  require("global_function.php");
  session_start();


  authorize();


  $id = $_REQUEST['id'];
  $sql = "SELECT * FROM users where id=$id";

  $stmt = $conn->prepare($sql);
  $stmt->execute();


  $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  $record=$stmt->fetch();

?>


<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">




<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

<center>
<form action="edit_action.php">
 <div style="justify-content:center; margin-top:5%; width: 30%; height:50%; border-radius:10px;background-color:#ADD8E6">
  <h1 style="margin-top:10%">Edit user</h1>

    <div class="mb-3" style="width:70%; margin-top:20px">
  <input value="<?php echo $record['id']; ?>" type="text" class="form-control" hidden name="id">
    </div>

  <div class="mb-3" style="width:70%; margin-top:20px">
  <input value="<?php echo $record['name']; ?>" type="text" class="form-control" autofocus name="name" placeholder="Enter name"> 
  </div>

  <div class="mb-3" style="width:70%">
  <input value="<?php echo $record['age']; ?>" type="text" class="form-control" autofocus name="age" placeholder="Enter age">
  </div>

  <div class="mb-3" style="width:70%">
  <input value ="<?php echo $record['city']; ?>" type="text" class="form-control" autofocus name="city" placeholder="Enter city"/>

  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  <button type="cancel" class="btn btn-danger">Cancel</button>
  </div>
</form>
