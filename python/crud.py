import pymysql
import csv
from prettytable import PrettyTable


conn = pymysql.connect(host = 'localhost',user = 'diituser',password = '%TGBbgt5',db = 'ecom')
cur = conn.cursor(pymysql.cursors.DictCursor);

def exportData():

    to_csv = showData();

    keys = to_csv[0].keys()
    file_path = 'users.csv';

    with open(file_path, 'w', newline='') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(to_csv)
        
    print(f"Data exported in {file_path}");

def deleteData():
    user_id = int(input("enter id to delete record: "))
    sql = f"delete from users where id ={user_id}";
    cur.execute(sql)
    conn.commit();
    showData();
 
def insertData():
    user_name = input("enter name: ")
    user_age = int(input("enter age: "))
    user_city = input("enter city: ")
    sql = f"INSERT INTO `users` (`id`, `name`, `age`, `city`, `added_at`, `updated_at`) VALUES (NULL, '{user_name}', '{user_age}', '{user_city}',NOW(),NOW())";
    cur.execute(sql)
    conn.commit();
    showData();


def updateData():
    user_id = int(input("enter id: "))
    user_name = input("enter name: ")
    user_age = int(input("enter age: "))
    user_city = input("enter city: ")
    sql = f"UPDATE `users` SET `name` = '{user_name}', `age` = '{user_age}', `city` = '{user_city}', `updated_at` = NOW() WHERE `users`.`id` ={user_id}";
    cur.execute(sql)
    conn.commit();
    showData();

def showData():
    sql = f"SELECT * FROM users ;"
    cur.execute(sql)
    records = cur.fetchall()
    #print(records)

    t = PrettyTable(['id','name','age','city','added_at','updated_at']);
    
    for user in records:
        t.add_row([user['id'],user['name'].title(),user['age'],user['city'],user['added_at'],user['updated_at']]);
        
    print(t) 
    return records;

option = None

while option != 0:

    print("\n=====================\n")
    print("1.show data")
    print("2.delete data")
    print("3.insert data")
    print("4.update data")
    print("5.export data")
    print("0.exit")
    print("\n=====================\n")

    option=int(input("choose option: "))

    if option == 1:
        showData();
    
    if option == 2:
        deleteData();
    
    if option == 3:
        insertData();
    
    if option == 4:
        updateData();

    if option == 5:
        exportData();
    
    if option == 0:
        print("exit")


